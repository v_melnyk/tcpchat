﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp
{
    public class UserModel
    {
        public int ID { get; set; }
        public string Nickname { get; set; }
        public string Password { get; set; }
    }
}
