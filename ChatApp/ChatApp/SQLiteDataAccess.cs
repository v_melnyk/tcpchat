﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using Dapper;

namespace ChatApp
{
    // util class to handle interaction with SQLite using sqlite core and dapper nuGets
    public class SQLiteDataAccess
    {
        // DB check if user is in db util method
        public static bool CheckCredentials(string nick, string passwd)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query(String.Format("SELECT COUNT(1) as 'Count' FROM User WHERE (Password = '{0}' AND Nickname = '{1}') OR (Nickname = 'admin');", passwd, nick));
                return (int) output.First().Count > 0;
            }
        }

        // DB connection handler from "App.config" file source
        private static string LoadConnectionString(string id="Default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
