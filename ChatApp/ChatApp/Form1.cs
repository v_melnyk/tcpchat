﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleTCP;

namespace ChatApp
{
    // Class for main server window
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // init simple TCP server from SimpleTcp nuGet 
        SimpleTcpServer server;

        private void Form1_Load(object sender, EventArgs e)
        {
            server = new SimpleTcpServer();  // creating new TCP server instance
            server.Delimiter = 0x0;  // setting delimiter to empty since we will be using "Environment.NewLine"
            server.StringEncoder = Encoding.UTF8;  // set encoding to utf8
            server.DataReceived += Server_DataReceived;  // init data reciever
        }

        // function to handle incoming message
        private void Server_DataReceived(object sender, SimpleTCP.Message e)
        {
            textLog.Invoke((MethodInvoker)delegate ()
            {
                string[] splitted = e.MessageString.Replace("\0", string.Empty).Split(';');  // split and sanitize string if string is for login
                if (splitted.Count() == 3 && splitted[0] == "[[LOGGING_IN]]")  // check id message is login message
                {
                    if (!SQLiteDataAccess.CheckCredentials(splitted[1], splitted[2]))  // check if login/password in db
                    {
                        // if user not in db assume intruder alert, shutdown!!!
                        textLog.AppendText("Unknown person connecting... Closing All Connections!!!");  // log server stopping
                        server.BroadcastLine("Unknown person connecting... Closing All Connections!!!");  // send message why server shut down
                        server.Stop();  // stop server
                    }

                }
                else
                {
                    // else chatting
                    textLog.AppendText(e.MessageString);  // log message in server window
                    textLog.AppendText(Environment.NewLine);  // new line
                    server.BroadcastLine(e.MessageString);  // send message to all connected users
                }
            }
            );
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            textLog.AppendText("Starting...");  //starting server message in log
            textLog.AppendText(Environment.NewLine);  // new line in log
            System.Net.IPAddress iP = System.Net.IPAddress.Parse(textHostIp.Text);  //get ip and port for server
            server.Start(iP, Convert.ToInt32(textPort.Text));  // start simple tcp server
            buttonOpen.Enabled = false;
            buttonClose.Enabled = true;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            if (server.IsStarted)
            {
                server.Stop();  // stop server
                buttonOpen.Enabled = true;
                buttonClose.Enabled = false;
            }
        }
    }
}
