﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatClient
{
    // utility class derived from tab page to show chat widgets in tab
    class ModTabPage : TabPage
    {
        private Form frm;
        public ModTabPage(ModFormPage frm_cont)
        {
            this.frm = frm_cont;
            this.Controls.Add(frm_cont.pnl);
            this.Text = frm_cont.Text;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                frm.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    // utility class to get panel with chat vidgets from form with panel containing chat vidgets
    public class ModFormPage : Form
    {
        public Panel pnl;
    }
}
