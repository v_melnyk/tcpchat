﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatClient
{
    // class for credentials
    public partial class Form2 : Form
    {
        DataTransfer transferDel;  // utility to transfer data from login form

        public Form2(DataTransfer del)
        {
            InitializeComponent();
            transferDel = del;  // utility to transfer data from login form
        }

        private void button1_Click(object sender, EventArgs e)
        {
            transferDel.Invoke(textInputIP.Text, textInputPort.Text, textInputNick.Text, textBox1.Text);  // utility to transfer data from login form
            Close();  // close login form
        }
    }
}
