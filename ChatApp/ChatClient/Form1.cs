﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using SimpleTCP;

namespace ChatClient
{
    public delegate void DataTransfer(string i, string p, string n, string pass);  // utility to read credentials from dialog window
    public partial class Form1 : Form
    {
        public DataTransfer transferDelegate;  // utility to read credentials from dialog window
        Form2 inputForm = null;  // var for input form dialog
        Form3 about = null;  // var for help page
        public string ip = null;  // var for ip string
        public string port = null;  // var for port string
        public string nickname = null;  // var for name string
        public string password = null;  // var for password string

        public Form1()
        {
            InitializeComponent();
            transferDelegate += new DataTransfer(ReceiveInput);  // transfer data from dialog
        }

        // handler for data transfer
        public void ReceiveInput(string i, string p, string n, string pass)
        {
            this.port = p;
            this.nickname = n;
            this.ip = i;
            this.password = pass;
        }

        // handler for connection option in toolbar
        private void newConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inputForm = new Form2(transferDelegate);  // new form for credentials
            inputForm.ShowDialog();  // show dialog with form for credentials
            tabControl1.TabPages.Add(new ModTabPage(new Form4(this.ip, this.port, this.nickname, this.password)));  // create new tab with chat connection
        }

        // handler for disconnect menu item
        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.TabPages.Remove(tabControl1.SelectedTab);
        }

        // show about dialog handler
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            about = new Form3();
            about.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            inputForm = new Form2(transferDelegate);  // start with login form
            inputForm.ShowDialog();  // show login form for start
            tabControl1.TabPages.Add(new ModTabPage(new Form4(this.ip, this.port, this.nickname, this.password)));  // create new chat tab
        }
    }
}


