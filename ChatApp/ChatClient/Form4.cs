﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleTCP;

namespace ChatClient
{
    // utility form for chat window to be used in tab
    public partial class Form4 : ModFormPage
    {
        public string nickname = null;  // var for name
        public string ip = null;  // var for ip
        public string port = null;  // var for port
        public string password = null;  // var for password
        public Form4(string i, string p, string nick, string pass)
        {
            // init vars
            this.Text = i;
            this.nickname = nick;
            this.ip = i;
            this.port = p;
            this.password = pass;
            InitializeComponent();
            this.pnl = panel1;

            client = new SimpleTcpClient();  // init simple tcp chat client
            client.Delimiter = 0x0;  // set delimiter to empty because we will use "Environment.NewLine"
            client.StringEncoder = Encoding.UTF8;  // set encoding to utf8
            client.DataReceived += Client_DataReceived;  // set data receiver

            client.Connect(this.ip, Int32.Parse(this.port));  // connect to tcp server

            client.WriteLine("[[LOGGING_IN]];" + this.nickname + ";" + this.password);  // send login message

            // set label
            if (String.IsNullOrEmpty(this.nickname))
            {
                label1.Text = "Connected to: " + this.ip + ":" + this.port;
            }
            else
            {
                label1.Text = "Connected to: " + this.ip + ":" + this.port + " As: " + this.nickname;
            }
        }

        SimpleTcpClient client;  // client instance from simple tcp

        // form not being called so not really needed
        private void Form4_Load(object sender, EventArgs e)
        {
            client = new SimpleTcpClient();
            client.Delimiter = 0x0;
            client.StringEncoder = Encoding.UTF8;
            client.DataReceived += Client_DataReceived;
        }

        // utility for data reciever
        private void Client_DataReceived(object sender, SimpleTCP.Message e)
        {
            textChat.Invoke((MethodInvoker)delegate ()
            {
                textChat.AppendText(e.MessageString);
                textChat.AppendText(Environment.NewLine);
            }
            );
        }

        // utility function to deal with sending messages
        private void buttonSend_Click_1(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.nickname))
            {
                client.WriteLine("Message: " + textMessage.Text);
            }
            else
            {
                client.WriteLine(this.nickname + ": " + textMessage.Text);
            }
            textMessage.Text = "";
        }
    }
}
